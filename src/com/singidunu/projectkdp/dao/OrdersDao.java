/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author luka0
 */
public class OrdersDao {
    private static OrdersDao instance = new OrdersDao();
    private OrdersDao(){
        
    };
    
    public static OrdersDao getInstance(){
        return instance;
        
    }
    
    public void create(Connection conn, Orders or) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try{
            preparedStatement = conn.prepareStatement("INSERT INTO `orders`(`OrderId`, `OrderDate`, `CustomerId`, `EmployeeId`, `ShipperId`) VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1,or.getOrderId());
            preparedStatement.setInt(2,or.getOrderDate());
            preparedStatement.setInt(3,or.getCustomerId());
            preparedStatement.setInt(4,or.getEmployeeId());
            preparedStatement.setInt(5,or.getShipperId());
            preparedStatement.executeUpdate();
        }finally{
            
            
        }
    }
    
    public void update (Connection conn, Orders or) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE orders WHERE orderId = ? ");
            preparedStatement.setInt(1,or.getOrderId());
            preparedStatement.executeUpdate();
            
          
        }
    
    public void delete (Connection conn, Orders or) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM orders WHERE orderId=?");
        preparedStatement.setInt(1,or.getOrderId());
        preparedStatement.executeUpdate();
        
        }
    
    public Orders find(Connection conn, int orderId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Orders or = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM orders WHERE orderId=?");
        preparedStatement.setInt(1, orderId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            or = new Orders (resultSet.getInt("OrderId"),resultSet.getInt("OrderDate"),resultSet.getInt("CustomerId"),resultSet.getInt("EmployeeId"),resultSet.getInt("ShipperId"));
            
        }
        
    
    return or;
    }
    
    public List<Orders> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Orders> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM orders");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Orders o1 = new Orders (resultSet.getInt("OrderId"),resultSet.getInt("OrderDate"),resultSet.getInt("CustomerId"),resultSet.getInt("EmployeeId"),resultSet.getInt("ShipperId"));
            lista.add(o1);
        }
        
    
    return (List<Orders>) lista;
    }
    
   
    
}
