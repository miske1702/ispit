/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Employees;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author luka0
 */
public class EmployeesDao {
    
    private static final EmployeesDao instance = new EmployeesDao();
    private EmployeesDao(){
    }
        public static EmployeesDao getInstance(){
            return instance;
        
    }
        public void create(Connection conn, Employees emp) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try{
            preparedStatement = conn.prepareStatement("INSERT INTO `employees`(`EmployeesID`, `LastName`, `FirstName`, `BirthDate`) VALUES (?,?,?,?)");
            preparedStatement.setInt(1,emp.getEmployeeId());
            preparedStatement.setString(2,emp.getLastName());
            preparedStatement.setString(3,emp.getFirstName());
            preparedStatement.setInt(4,emp.getDate());
            preparedStatement.executeUpdate();
        }finally{
            
            
        }
    }
    
    public void update (Connection conn, Employees emp) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE employees WHERE employeeId = ? ");
            preparedStatement.setInt(1,emp.getEmployeeId());
            preparedStatement.executeUpdate();
            
          
        }
    
    public void delete (Connection conn,Employees emp) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM employees WHERE employeeId=?");
        preparedStatement.setInt(1,emp.getEmployeeId());
        preparedStatement.executeUpdate();
        
        }
    
    public Employees find(Connection conn, int employeeId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Employees emp = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM employees WHERE employeeId=?");
        preparedStatement.setInt(1, emp.getEmployeeId());
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            emp = new Employees(resultSet.getInt("EmployeesID"),resultSet.getString("LastName"),resultSet.getString("FirstName"),resultSet.getInt("BirthDate"));
            
        }
        
    
    return emp;
    }
    
    public List<Employees> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Employees> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM employees");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Employees e1 = new Employees(resultSet.getInt("EmployeeId"),resultSet.getString("LastName"),resultSet.getString("FirstName"),resultSet.getInt("BirthDate"));
            lista.add(e1);
            
        }
        
    
    return (List<Employees>) lista;
    }
    
}
