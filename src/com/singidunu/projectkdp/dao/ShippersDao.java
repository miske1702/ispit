/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Shippers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author luka0
 */
public class ShippersDao {
    private static final ShippersDao instance = new ShippersDao();
    private ShippersDao(){
    }
    
    public static ShippersDao getInstance(){
        return instance;
    }
    
    public void create(Connection conn, Shippers ship) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try{
            preparedStatement = conn.prepareStatement("INSERT INTO `shippers`(`ShipperID`, `ShipperName`, `Phone` VALUES (?,?,?)");
            preparedStatement.setInt(1,ship.getShipperId());
            preparedStatement.setString(2,ship.getShipperName());
            preparedStatement.setInt(3,ship.getPhone());
            
            preparedStatement.executeUpdate();
        }finally{
            
            
        }
    }
    
    public void update (Connection conn, Shippers ship) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE shippers WHERE shipperId = ? ");
            preparedStatement.setInt(1,ship.getShipperId());
            preparedStatement.executeUpdate();
            
          
        }
    
    public void delete (Connection conn, Shippers ship) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM shippers WHERE shipperId=?");
        preparedStatement.setInt(1,ship.getShipperId());
        preparedStatement.executeUpdate();
        
        }
    
    public Shippers find(Connection conn, int shipperId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Shippers ship = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM shippers WHERE shipperId=?");
        preparedStatement.setInt(1, shipperId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            ship = new Shippers (resultSet.getInt("ShipperID"),resultSet.getString("ShipperName"),resultSet.getInt("Phone"));
            
        }
        
    
    return ship;
    }
    
    public List<Shippers> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Shippers> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM shippers");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Shippers ship1 = new Shippers (resultSet.getInt("ShipperID"),resultSet.getString("ShipperName"),resultSet.getInt("Phone"));
            lista.add(ship1);
        }
        
    
    return (List<Shippers>) lista;
    }
    
}
