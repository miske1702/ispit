/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.OrderDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author luka0
 */
public class OrderDetailsDao {
    private static OrderDetailsDao instance = new OrderDetailsDao();
    
    private OrderDetailsDao(){
        
    }
    
    public static OrderDetailsDao getInstance(){
        return instance;
    }
    
    public void create(Connection conn, OrderDetails od) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
    

    try{
            preparedStatement = conn.prepareStatement("INSERT INTO `orderDetail`(`OrderDetailsID`, `OrderID`, `ProductID`, `Quantity`) VALUES (?,?,?,?)");
            preparedStatement.setInt(1,od.getOrderDetailsId());
            preparedStatement.setInt(2,od.getOrderId());
            preparedStatement.setInt(3,od.getProductId());
            preparedStatement.setInt(4,od.getQuantity());
            
        }finally{
            
            
        }
    }
    
     public void update (Connection conn, OrderDetails od) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE customers WHERE customerId = ? ");
            preparedStatement.setInt(1,od.getOrderDetailsId());
            preparedStatement.executeUpdate();
            
          
        }
     
     public void delete (Connection conn, OrderDetails od) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM orderDetails WHERE orderDetailsId=?");
        preparedStatement.setInt(1,od.getOrderDetailsId());
        preparedStatement.executeUpdate();
        
        }
     
      public OrderDetails find(Connection conn, int orderDetailsId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        OrderDetails od = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM orderDetails WHERE orderDetailsId=?");
        preparedStatement.setInt(1, orderDetailsId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            od = new OrderDetails(resultSet.getInt("OrderDetailsID"),resultSet.getInt("OrderID"),resultSet.getInt("ProductID"),resultSet.getInt("Quantity"));
            
        }
        return od;

}
      public List<OrderDetails> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<OrderDetails> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM orderDetails");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            OrderDetails od1 = new OrderDetails (resultSet.getInt("OrderDetailsID"),resultSet.getInt("OrderID"),resultSet.getInt("ProductID"),resultSet.getInt("Quantity"));
            lista.add(od1);
        }
        
    
    return (List<OrderDetails>) lista;
    }
}
