/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Products;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author luka0
 */
public class ProductsDao {
    private static ProductsDao instance = new ProductsDao();
    private ProductsDao(){
        
    };
    
    public static ProductsDao getInstance(){
        return instance;
    }
    
    public void create(Connection conn, Products pro) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try{
            preparedStatement = conn.prepareStatement("INSERT INTO `products`(`ProductID`, `ProductName`, `SupplierID`, `ProductCategory`, `PricePerUnit`) VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1,pro.getProductId());
            preparedStatement.setString(2,pro.getProductName());
            preparedStatement.setInt(3,pro.getSupplierId());
            preparedStatement.setString(4,pro.getProductCategory());
            preparedStatement.setInt(5,pro.getPricePerUnit());
            preparedStatement.executeUpdate();
        }finally{
            
            
        }
    }
    
    public void update (Connection conn, Products pro) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE products WHERE productId = ? ");
            preparedStatement.setInt(1,pro.getProductId());
            preparedStatement.executeUpdate();
            
          
        }
    
     public void delete (Connection conn, Products pro) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM products WHERE productId=?");
        preparedStatement.setInt(1,pro.getProductId());
        preparedStatement.executeUpdate();
        
        }
     
     public Products find(Connection conn, int productId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Products pro = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM products WHERE productId=?");
        preparedStatement.setInt(1, productId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            pro = new Products (resultSet.getInt("ProductID"),resultSet.getString("ProductName"),resultSet.getInt("SupplierID"),resultSet.getString("ProductCategory"),resultSet.getInt("PricePerUnit"));
            
        }
        
    
    return pro;
    }
      public List<Products> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Products> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM suppliers");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Products p1 = new Products (resultSet.getInt("ProductID"),resultSet.getString("ProductName"),resultSet.getInt("SupplierID"),resultSet.getString("ProductCategory"),resultSet.getInt("PricePerUnit"));
            lista.add(p1);  
        }
        
    
    return (List<Products>) lista;
    }
}
