/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Customer;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author luka0
 */
public class CustomerDao {
    private static CustomerDao instance = new CustomerDao();
    private CustomerDao(){
        
    };
    
    public static CustomerDao getInstance(){
        return instance;
    }
    
    public void create(Connection conn, Customer cust ) throws SQLException{
        PreparedStatement preparedStatement = null;
        
        
        
            preparedStatement =conn.prepareStatement("INSERT INTO `customers`(`CustomerID` ,`CustomerName` ,`ContactPerson`,`Address`,`City`,`PostCode`,`Country`) VALUES (?,?,?,?,?,?,?) ");
            preparedStatement.setInt(1,cust.getCustomerId());
            preparedStatement.setString(2,cust.getCustomerName());
            preparedStatement.setString(3,cust.getContactPerson());
            preparedStatement.setString(4,cust.getAdress());
            preparedStatement.setString(5,cust.getCity());
            preparedStatement.setInt(6,cust.getPostCode());
            preparedStatement.setString(7,cust.getCountry());
            
      
    }
    public void update(Connection conn, Customer cust)throws SQLException{
        PreparedStatement preparedStatement=null;
        
        preparedStatement = conn.prepareStatement("UPDATE customers WHERE customerID=?");
        preparedStatement.setInt(1,cust.getCustomerId());
        preparedStatement.executeUpdate();
        
        
        
    }
    
    
    public void delete(Connection conn, Customer cust ) throws SQLException{
        PreparedStatement preparedStatement=null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM customers WHERE CustomerID=?");
        preparedStatement.setInt(1,cust.getCustomerId());
        preparedStatement.executeUpdate();
        
      
    }
    
    public Customer find(Connection conn , int customerId)throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        Customer cust = null;
        
        preparedStatement = conn.prepareStatement("SELECT *FROM customers WHERE customerId=?");
        preparedStatement.setInt(1, customerId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            cust = new Customer(resultSet.getInt("CustomerID"),resultSet.getString("CustomerName"),resultSet.getString("ContactPerson"),resultSet.getString("Address"),resultSet.getString("City"),resultSet.getInt("PostCode"),resultSet.getString("Country"));
        }
        return cust;
    }
    
    public List<Customer> findAll(Connection conn)throws SQLException{
        PreparedStatement preparedStatement= null;
        ResultSet resultSet=null;
        List<Customer> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT *FROM customers");
        resultSet=preparedStatement.executeQuery();
        
        while(resultSet.next()){
            Customer c = new Customer(resultSet.getInt("CustomerId"),resultSet.getString("CustomerName"),resultSet.getString("ContactPerson"),resultSet.getString("Address"),resultSet.getString("City"),resultSet.getInt("PostCode"),resultSet.getString("Country"));
            lista.add(c);
        }
        return (List<Customer>)lista;
    }
    
}
