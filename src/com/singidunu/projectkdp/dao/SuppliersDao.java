/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.dao;
import com.singidunu.projectkdp.deo.Suppliers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author luka0
 */
public class SuppliersDao {
    private static final SuppliersDao instance = new SuppliersDao();
    private SuppliersDao(){
    }
    
    public static SuppliersDao getInstance(){
        return instance;
    }
    
    public void create(Connection conn, Suppliers sup) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try{
            preparedStatement = conn.prepareStatement("INSERT INTO `suppliers`(`SupplierID`, `SupplierName`, `ContactPerson`, `Address`, `City`, `PostCode`, `Country`, `Phone` ) VALUES (?,?,?,?,?,?,?,?)");
            preparedStatement.setInt(1,sup.getSupplierId());
            preparedStatement.setString(2,sup.getSupplierName());
            preparedStatement.setString(3,sup.getContactPerson());
            preparedStatement.setString(4,sup.getAddress());
            preparedStatement.setString(5,sup.getCity());
            preparedStatement.setInt(6,sup.getPostCode());
            preparedStatement.setString(7,sup.getCountry());
            preparedStatement.setInt(8,sup.getPhone());
            preparedStatement.executeUpdate();
        }finally{
            
            
        }
    }
    
    public void update (Connection conn, Suppliers sup) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        
            preparedStatement = conn.prepareStatement("UPDATE suppliers WHERE supplierId = ? ");
            preparedStatement.setInt(1,sup.getSupplierId());
            preparedStatement.executeUpdate();
            
          
        }
    
    public void delete (Connection conn, Suppliers sup) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        preparedStatement = conn.prepareStatement("DELETE FROM suppliers WHERE supplierId=?");
        preparedStatement.setInt(1,sup.getSupplierId());
        preparedStatement.executeUpdate();
        
        }
    
    public Suppliers find(Connection conn, int supplierId) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Suppliers sup = null;
        
        preparedStatement = conn.prepareStatement("SELECT * FROM suppliers WHERE supplierId=?");
        preparedStatement.setInt(1, supplierId);
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            sup = new Suppliers (resultSet.getInt("supplierId"),resultSet.getString("supplierName"),resultSet.getString("contactPerson"),resultSet.getString("Address"),resultSet.getString("City"),resultSet.getInt("PostCode"),resultSet.getString("Country"),resultSet.getInt("Phone"));
            
        }
        
    
    return sup;
    }
    
    public List<Suppliers> findAll(Connection conn) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Suppliers> lista = new LinkedList();
        
        preparedStatement = conn.prepareStatement("SELECT * FROM suppliers");
        
        resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            Suppliers s1 = new Suppliers (resultSet.getInt("supplierId"),resultSet.getString("supplierName"),resultSet.getString("contactPerson"),resultSet.getString("address"),resultSet.getString("city"),resultSet.getInt("postCode"),resultSet.getString("Country"),resultSet.getInt("Phone"));
            lista.add(s1);  
        }
        
    
    return (List<Suppliers>) lista;
    }
    
}
