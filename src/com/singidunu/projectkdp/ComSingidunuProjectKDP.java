/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.singidunu.projectkdp.dao.*;
import com.singidunu.projectkdp.deo.*;
/**
 *
 * @author luka0
 */
public class ComSingidunuProjectKDP {
     private static final ComSingidunuProjectKDP instance = new ComSingidunuProjectKDP();
    ComSingidunuProjectKDP(){
    }
    
    public static ComSingidunuProjectKDP getInstance(){
        return instance;
    }
    
    private Connection connection = null;
    private PreparedStatement preparedStatement = null;
    
    private ResultSet resultSet = null;
    
    public void CustomerOrders() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement("SELECT * FROM warehousee.customers INNER JOIN warehousee.orders ON customers.customerId = orders.orderId ORDER BY customerName");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("CustomerOrders");
           System.out.println("CustomerID" + "CustomerName" + " OrderID");
           while(resultSet.next()){
               int id = resultSet.getInt("customerId");
               String ime = resultSet.getString("customerName");
               int ordId = resultSet.getInt("orderId");
               
               
               System.out.println(id + "           " + ime+"           " + ordId+ "          " + "\n"  );
               
           }

    
}
    
    public void ProductSupplier() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement("SELECT Products.ProductName, Suppliers.SupplierName FROM Suppliers INNER JOIN Products ON Suppliers.SupplierId=Products.SupplierId ");
           resultSet = preparedStatement.executeQuery();
           
            System.out.println("ProductSupplier:");
            System.out.println("ProductName" + " SupplierName");
           while(resultSet.next()){
               
               String prodName = resultSet.getString("ProductName");
               String supName = resultSet.getString("SupplierName");
               
              
               System.out.println(prodName + "           " + supName+ "           " + "\n"  );
               
           }
    }
           
   
    public void ProductShipper() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement("SELECT Products.ProductName, Shippers.ShipperName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Shippers ON Orders.ShipperId=Shippers.ShipperId WHERE Shippers.ShipperId = 1; ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("ProductShipper:");
            System.out.println("ProductName" + " ShipperName");
           while(resultSet.next()){
               
               String prodName = resultSet.getString("ProductName");
               String shipName = resultSet.getString("ShipperName");
               
              
               System.out.println(prodName + "           " + shipName+ "           " + "\n"  );
               
           }
    }
          
    public void UkupnaCenaNarudzbina() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT SUM(PricePerUnit) AS UkupnaCena FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("UkupnaCenaNarudzbina:");
            System.out.println("UkupnaCena");
           while(resultSet.next()){
               
               int ukupnaCena = resultSet.getInt("UkupnaCena");

               System.out.println(ukupnaCena + "           " +"\n"  );
               
           }
   }
           
    public void CustomerCenaNarudzbina(/*Connection conn, int customerId*/) throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
           
    
           preparedStatement = connection.prepareStatement(" SELECT SUM(PricePerUnit) AS CenaOrdera, Customers.CustomerId FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Customers ON Orders.CustomerId=Customers.CustomerId WHERE Customers.CustomerId = 2; ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("CustomerCenaNarudzbina:");
            System.out.println("CenaOredera" + "CustomerId");
           while(resultSet.next()){
               
               int cenaOrdera = resultSet.getInt("CenaOrdera");
               int custID = resultSet.getInt("CustomerId");

               System.out.println(cenaOrdera + "           " + custID + "       " + "\n"  );
               
           }
    }
           
    public void ShipperCenaNarudzbina() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT SUM(PricePerUnit) AS CenaOrderaShipper, Shippers.ShipperId FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Shippers ON Orders.ShipperId=Shippers.ShipperId WHERE Shippers.ShipperId=3; ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("ShipperCenaNarudzbina:");
            System.out.println("CenaOrderaShipper" + "ShipperId");
           while(resultSet.next()){
               
               int cenaShipp = resultSet.getInt("CenaOrderaShipper");
               int shippID = resultSet.getInt("ShipperId");

               System.out.println(cenaShipp + "           " + shippID + "         " + "\n"  );
               
           }
    }
           
    public void SupplierCenaNarudzbina() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT SUM(PricePerUnit) AS CenaOrederaSupplier, Suppliers.SupplierId FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Suppliers ON Products.SupplierID=Suppliers.SupplierID WHERE Suppliers.SupplierId=4; ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("SupplierCenaNarudzbina:");
            System.out.println("CenaOrederaSupplier" + "SupplierId");
           while(resultSet.next()){
               
               int cenaSupp = resultSet.getInt("CenaOrederaSupplier");
               int suppID = resultSet.getInt("SupplierId");

               System.out.println(cenaSupp + "           " + suppID + "          " + "\n"  );
               
           }
           }
           
    public void NajvecaVrednostRobe() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT MAX(Products.PricePerUnit) AS NajvecaVrednost, Employees.FirstName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Employees ON Orders.EmployeeId=Employees.EmployeeId ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("NajvecaVrednostRobe:");
            System.out.println("NajvecaVrednost" + "FirstName");
           while(resultSet.next()){
               
               int najvecaVred = resultSet.getInt("NajvecaVrednost");
               String empFirstName = resultSet.getString("FirstName");

               System.out.println(najvecaVred + "           " + empFirstName + "             "+ "\n"  );
               
           }
}
     public void MaxOrderedProducts() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT OrderDetails.Quantity, Products.ProductName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId WHERE (SELECT MAX(OrderDetails.Quantity) FROM OrderDetails) ORDER BY OrderDetails.Quantity DESC LIMIT 3; ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("MaxOrderedProducts:");
            System.out.println("Quantity" + "ProductName");
           while(resultSet.next()){
               
               int maxNarudzbine = resultSet.getInt("Quantity");
               String prodName = resultSet.getString("ProductName");

               System.out.println(maxNarudzbine + "           " + prodName + "             "+ "\n"  );
               
           }
}
     public void NajboljaCustomera() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT SUM(Products.PricePerUnit) AS CenaOrdera, Customers.CustomerName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Customers ON Orders.CustomerId=Customers.CustomerId GROUP BY Customers.CustomerName HAVING SUM(Products.PricePerUnit) = ( SELECT MAX(CenaOrdera) FROM ( SELECT SUM(Products.PricePerUnit) AS CenaOrdera, Customers.CustomerName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Customers ON Orders.CustomerId=Customers.CustomerId GROUP BY Customers.CustomerName) AS O) ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("NajboljaCustomera:");
            System.out.println("CenaOrdera" + "CustomerName");
           while(resultSet.next()){
               
               int cenaOrdera = resultSet.getInt("CenaOrdera");
               String custName = resultSet.getString("CustomerName");

               System.out.println(cenaOrdera + "           " + custName + "             "+ "\n"  );
               
           }
}
      public void SupplierNajviseOrdera() throws ClassNotFoundException, SQLException{
           Class.forName("com.mysql.jdbc.Driver"); 
           connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehousee", "root", "admin");
    
           preparedStatement = connection.prepareStatement(" SELECT COUNT(Orders.OrderId) AS BrojOrdera, Suppliers.SupplierName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Suppliers ON Products.SupplierId=Suppliers.SupplierId GROUP BY Suppliers.SupplierName HAVING COUNT(Orders.OrderId) = ( SELECT MAX(BrojOrdera) FROM ( SELECT COUNT(Orders.OrderId) AS BrojOrdera, Suppliers.SupplierName FROM Products INNER JOIN OrderDetails ON Products.ProductId=OrderDetails.ProductId INNER JOIN Orders ON OrderDetails.OrderId=Orders.OrderId INNER JOIN Suppliers ON Products.SupplierId=Suppliers.SupplierId WHERE (SELECT MAX(Orders.OrderID) FROM Orders) GROUP BY Suppliers.SupplierName) AS O) ");
           resultSet = preparedStatement.executeQuery();
           
           System.out.println("SupplierNajviseOrdera:");
            System.out.println("BrojOrdera" + "SupplierName");
           while(resultSet.next()){
               
               int brojOrdera = resultSet.getInt("BrojOrdera");
               String suppName = resultSet.getString("SupplierName");

               System.out.println(brojOrdera + "           " + suppName + "             "+ "\n"  );
               
           }
}
    
}
