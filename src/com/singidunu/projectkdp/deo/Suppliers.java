/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.deo;

/**
 *
 * @author luka0
 */
public class Suppliers {
    private int supplierId;
    private String supplierName;
    private String contactPerson;
    private String address;
    private String city;
    private int postCode;
    private String country;
    private int phone;
    
    public Suppliers (int supplierId, String supplierName , String contactPerson, String adress , String city, int postCode, String country, int phone){
        this.supplierId=supplierId;
        this.supplierName=supplierName;
        this.address=adress;
        this.contactPerson=contactPerson;
        this.city=city;
        this.postCode=postCode;
        this.country=country;
        this.phone=phone; 
        
    }   

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Suppliers{" + "supplierId=" + supplierId + ", supplierName=" + supplierName + ", contactPerson=" + contactPerson + ", address=" + address + ", city=" + city + ", postCode=" + postCode + ", country=" + country + ", phone=" + phone + '}';
    }
    
    
    
    
}
