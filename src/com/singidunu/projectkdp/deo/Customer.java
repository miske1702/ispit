/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.deo;

/**
 *
 * @author luka0
 */
public class Customer {
    private int customerId;
    private String customerName;
    private String contactPerson;
    private String adress;
    private String city;
    private int postCode;
    private String country;
    
    public Customer (int customerId, String customerName, String 
            contactPerson, String adress, String city , int postCode, String country){
        this.customerId=customerId;
        this.customerName=customerName;
        this.contactPerson=contactPerson;
        this.adress=adress;
        this.city=city;
        this.postCode=postCode;
        this.country=country;
          }

    public int getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public String getAdress() {
        return adress;
    }

    public String getCity() {
        return city;
    }

    public int getPostCode() {
        return postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerId=" + customerId + ", customerName=" + customerName + ", contactPerson=" + contactPerson + ", adress=" + adress + ", city=" + city + ", postCode=" + postCode + ", country=" + country + '}';
    }
    
    
     
     
    
    

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }
 
 
    
    
}
