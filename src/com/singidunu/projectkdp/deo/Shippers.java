/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunu.projectkdp.deo;

/**
 *
 * @author luka0
 */
public class Shippers {
    private int shipperId;
    private String shipperName;
    private int phone;
    
    public Shippers(int shipperId,String shipperName, int phone){
        this.phone=phone;
        this.shipperId=shipperId;
        this.shipperName=shipperName;
    }

    public int getShipperId() {
        return shipperId;
    }

    public void setShipperId(int shipperId) {
        this.shipperId = shipperId;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Shippers{" + "shipperId=" + shipperId + ", shipperName=" + shipperName + ", phone=" + phone + '}';
    }
    
}
